shell-alc
=========

### Installation
*  `chmod +x ./alc`
*  `sudo cp ./alc /usr/local/bin/alc`

#### Dependencies
*  w3m

### Usage
*  `alc WORD_YOU_WANT_TO_SEARCH`
*  Your search log will be written in ~/words.txt

